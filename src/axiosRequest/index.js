import axios from 'axios'

const service = axios.create({
    baseURL: "", // 默认请求地址
    timeout: 1000 * 60 // 请求时间
})

//设置请求拦截
service.interceptors.request.use(
    config => {
        //......

        return config
    },
    error => {
        console.log(error)
        return Promise.reject(error)
    }
)

// 设置响应拦截
service.interceptors.response.use(
    response => {
        const res = response.data
        //状态码非200
        if (res.code !== 200) {
            alert("error")
            return Promise.reject(new Error('Error'))
        } else {
            // 状态码200
            return res
        }
    },
    error => {
        console.log('err' + error)
        alert("error")
        return Promise.reject(error)
    }
)

// 抛出
export default service









