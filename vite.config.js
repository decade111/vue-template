import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  assetsDir: "static",
  productionSourceMap: false,
  devServer: {
    port: 3000  // 端口
  },
  plugins: [vue()]
})
